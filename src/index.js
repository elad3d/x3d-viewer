//React and Fragment are imported for compiler sake
import React, {  useRef,useEffect, memo, useState, Fragment } from 'react';
import { interval, timer } from 'rxjs';
import { takeUntil, takeWhile } from 'rxjs/operators';
import { resolveViewpoints, resolveErrorsByShape, updateShape, getAproxCogFromInline, getBoundingBoxFromInline, getFallbackViewpoint, convertDegreesToRadians } from './helpers';
import Axes from './components/Axes';
import './MyX3DViewer.css';
import ViewpointButtons from './components/ViewpointButtons';
import Viewpoints from './components/Viewpoints';

export const MyX3DViewer = memo(({
			conf,
			clickHandler,
			mouseoverHandler,
			mouseoutHandler,
			errorHandler=console.error,
			timeoutSecs=7,
			showDefaultViewpoints=true,
			showCustomViewpoints=true,
			rotation
		}) => {

	const refScene = useRef(null);
	const refInline = useRef(null);

  	const [error, setError] = useState(false);
	const [defaultVps, setDefaultVps] = useState([]);
	const [customVps, setCustomVps] = useState([]);
	const [showVpButtons, setShowVpButtons] = useState(true);
	const [cog, setCog] = useState(conf.cog);
	const [bbox, setBbox] = useState(conf.boundingBox);
	const [loading, setLoading] = useState(true);
	const [rotationValue, setRotationValue] = useState(null);

    useEffect(() => {
		window.x3dom && window.x3dom.reload();
		//check every 250ms if model is already loaded, when it does, it calls the complete callback
		//however if after 5s it hasn't loaded, it shows an error
		interval(250)
			.pipe(takeUntil(timer(timeoutSecs*1000)))
			.pipe(takeWhile(()=>!refInline.current.querySelectorAll("Shape").length))
			.subscribe({complete: ()=>{
				if (!refInline.current.querySelectorAll("Shape").length){
					setError(true);
					errorHandler(`Model could not be loaded, please check ${conf.url} format and try again.`);
				} else {
					//transpose error list into shape list with errors
					const shapes = resolveErrorsByShape(conf.errors);
					//once I have my "shape map", I process the colors for each one
					shapes.forEach(shape => updateShape(refInline, shape, clickHandler, mouseoverHandler, mouseoutHandler));
					//Take bbox and cog from conf props, or resolve with x3dom api
					const _bbox = conf.boundingBox || getBoundingBoxFromInline(refInline);
					setBbox(_bbox);
					const _cog = conf.cog || getAproxCogFromInline(refInline);
					setCog(_cog);
					//now that everyting is set right according to the conf object, we get the viewpoints
					const _defaultVps = resolveViewpoints(_cog, _bbox);
					setCustomVps(showCustomViewpoints && conf.viewpoints?.length ? conf.viewpoints : []);
					setDefaultVps(showDefaultViewpoints ? _defaultVps : (showCustomViewpoints && conf.viewpoints?.length ? [] : getFallbackViewpoint(_cog, _bbox) ));
					setShowVpButtons(customVps.length || showDefaultViewpoints);
					if (rotation?.length === 4 ){
						const [cosx, cosy, cosz, angleDegrees] = rotation;
						const angleRadians = convertDegreesToRadians(angleDegrees);
						const rotVal = [cosx, cosy, cosz, angleRadians]
						setRotationValue(rotVal);
					}

					setLoading(false);
				}
			}});
	}, [conf.cog, conf.url, conf.errors, conf.boundingBox, clickHandler, mouseoverHandler, mouseoutHandler, showCustomViewpoints, showDefaultViewpoints, rotation]);

  return (
		<div>
			{ !error && showVpButtons && (<ViewpointButtons defaults={defaultVps} custom={customVps}/>) }
			<x3d is="x3d">
				<scene  is="x3d" ref={refScene} >
					{ !error && <Viewpoints defaults={defaultVps} custom={customVps}/>}
  					{ bbox && <Axes bbox={bbox} show={!loading && conf.showXYZ} /> }
					<transform is="x3d" rotation={rotationValue}>
						<inline is="x3d" ref={refInline} nameSpaceName="Model" mapDEFToID="true" url={conf.url}/>
					</transform>
				</scene>
			</x3d>
		</div>) ;
});
