import { memo, useState, useCallback } from "react";
import React from 'react'
import { MyX3DViewer } from "x3d-viewer";


function ViewerWrapper() {
	const [conf, setConf] = useState({

		errors: [
			{
			  color: [0.737374, 0.1111, 0.243245676543],
			  desc: 'Its a hole',
			  id: 'A',
			  name: 'HOLES',
			  visible: true
			},
			{
			  color: [0.737374, 0.1111, 0.243245676543],
			  desc: 'Its an overhanging area',
			  id: 'B',
			  name: 'OVERHANGING',
			  visible: true
			},
			{
			  color: [0.737374, 0.1111, 0.243245676543],
			  desc: 'Its a thin wall',
			  id: 'C',
			  name: 'WT',
			  visible: true
			},
			{
			  color: [0.737374, 0.1111, 0.243245676543],
			  desc: 'Its a hole and overhanging area',
			  id: 'A_B',
			  name: 'HOLE_OVERHANGING',
			  visible: true
			},
			{
			  color: [0.737374, 0.1111, 0.243245676543],
			  desc: 'Its a hole and thin wall area',
			  id: 'A_C',
			  name: 'HOLE_WT',
			  visible: true
			},
			{
			  color: [0.737374, 0.1111, 0.243245676543],
			  desc: 'Its an overhanging and thin wall area',
			  id: 'B_C',
			  name: 'OVERHANGING_WT',
			  visible: true
			},
			{
			  color: [0.737374, 0.1111, 0.243245676543],
			  desc: 'Its a hole, overhanging and thin wall area',
			  id: 'A_B_C',
			  name: 'HOLE_OVERHANGING_WT',
			  visible: true
			}],
		  id: '0',
		  showXYZ: true,
		url: `http://localhost:3000/4cfa25df-ccea-4a39-bd01-944823732e26.x3d`

	});

	const [active, setActive] = useState(null);
	const [over, setOver] = useState(null);
	const [rotate, setRotate] = useState(null);

	const clickHandler = useCallback(
		(payload) => {
			setActive(payload);
		},[]
	)
	const mouseoverHandler = useCallback(
		(payload) => {
			setOver(payload.id);
		},[]
	)
	const mouseoutHandler = useCallback(
		(payload) => {
			setOver(null)
		},[]
	)
	const toggleAxes = useCallback(
		() => {
			setConf((config)=>({
				...config,
				showXYZ: !config.showXYZ
			}))
		},[]
	)
	const toggleError = useCallback(
		(errorId) => {
			setConf(()=>({
				...conf,
				errors: conf.errors.map(e=>({...e, visible: e.id===errorId ? !e.visible : e.visible}))
			}))
		},[conf]
  )

  const getColor = useCallback((color)=>{
		const colorArr = (color?.length ? color : [0,0,0]);
		const colorHex = [colorArr[0]*256, colorArr[1]*256, colorArr[2]*256];

		return `rgb(${colorHex.join(",")})`;
	}, []);

	const errorHandler = useCallback(err=>alert(err), []);

	const prompRotate = useCallback(x=>{
		setRotate(prompt("Enter rotation value").split(",").map(i=>+i));
	}, [])

	return (
		<>
			<header className="App-header">
				<h2 onClick={prompRotate}>X3D File Viewer</h2>
			</header>
			<div className="content">
				<button id="axestoggler" type="button" onClick={toggleAxes}>Toggle Axes</button>
				<MyX3DViewer
					conf={conf}
					mouseoutHandler={mouseoutHandler}
					mouseoverHandler={mouseoverHandler}
					clickHandler={clickHandler}
					errorHandler={errorHandler}
					timeoutSecs={10}
					showDefaultViewpoints={false}
					rotation={rotate}></MyX3DViewer>
			</div>
			<footer>
				{active && (<h5>{active.id}</h5>)}
				<ul className="error-list">
					{(conf?.errors || []).map((error)=>(
						<li key={error.id} style={{color: getColor(error.color)}}>
							<span className={`error-desc toggle-${error.visible ? 'on' : 'off'}`}> {error.desc} </span> |
					    <strong className={`toggle-${error.visible ? 'on' : 'off'}`} onClick={()=>toggleError(error.id)}>Toggle {error.visible ? 'off' : 'on'}</strong >
						</li>
					))}
				</ul>
			</footer>
		</>
	)


}

export default memo(ViewerWrapper);
