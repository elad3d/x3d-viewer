import React from 'react'

import 'x3d-viewer/dist/index.css'
import ViewerWrapper from './components/ViewerWrapper'
import './App.css';

const App = () => {
  return (
      <div className="App">
        <ViewerWrapper />
      </div>
  )
}

export default App
