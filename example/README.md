This example was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

It is linked to the x3d-viewer package in the parent directory for development purposes.

After you ran `npm run-script build` on the root path you can `cd example`, run `npm run example`!